#include <iostream>
#include "socket.h"

int main(int argc, char* argv[])
{
    if (argc < 2)
    {
        std::cout << " Error, missing url ";
        return 0;
    }

    HTTPSocket socket = HTTPSocket();
    socket.addHTTPRequest(argv[1], "GET", {});

    return 0;
}
