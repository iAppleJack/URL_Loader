#include <string>
#include <vector>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <iostream>
#include <netdb.h>
#include <netinet/in.h>
#include <stdio.h>
#include <strings.h>
#include <fstream>
#include <cstring>
#include <stdlib.h>
#include <unistd.h>
class HTTPSocket
{
private:
    int socketId;
    struct sockaddr_in addr;
    struct hostent* raw_host;

    std::string protocol;
    std::string domain;
    std::string path;
    std::string filename;
    std::string query;
    std::string fragment;

    void addProtocol(const std::string &URL);
    void addDomain(const std::string &URL);
    void addPath(const std::string &URL);
    void addName(const std::string &URL);


    void parseContentType(const std::string &responce);
    void getFile(const int &bSize);

public:
    HTTPSocket();
    ~HTTPSocket();

    void addHTTPRequest(std::string url, std::string type, std::vector<std::string> params);

};
