#include "socket.h"


void HTTPSocket::addProtocol(const std::string& URL)
{
    protocol = "";
    for(size_t i = 0; i < URL.size() - 1; i++)
    {
        if( URL[i] != '/' || URL[i+1] != '/'  )
        {
            protocol += URL[i];
        }
        else
        {
            protocol += "//";
            break;
        }
    }

}

void HTTPSocket::addDomain(const std::string &URL)
{
    domain = "";
    for (size_t i = protocol.size(); i < URL.size(); ++i)
    {
        if( URL[i] != '/' )
            domain += URL[i];
        else
            break;
    }
}

void HTTPSocket::addPath(const std::string &URL)
{
    std::string part = "";
    path       = "";

    for (size_t i = protocol.size() + domain.size() + 1; i < URL.size(); ++i)
    {
        part += URL[i];
        if (URL[i] == '/')
        {
            path += part;
            part        = "";
        }
        if (URL[i] == '?' || URL[i] == '#' || URL[i] == '.')
            break;
    }

}

void HTTPSocket::addName(const std::string &URL)
{
    filename = "";
    for (size_t i = protocol.size() + domain.size() + path.size() + 1; i < URL.size(); ++i)
    {
        if (URL[i] == '?' || URL[i] == '#')
            break;
        filename += URL[i];
    }
}



HTTPSocket::HTTPSocket()
{

}

HTTPSocket::~HTTPSocket()
{
}

void HTTPSocket::getFile(const int &bSize) {
    if (filename.empty())
        filename = "index.html";
    std::ofstream file(filename, std::ofstream::binary | std::ofstream::out);

    char buffer[bSize];
    ssize_t body_received;

    body_received = recv(socketId, buffer, bSize, 0);


    int bodySize = 0;
    bodySize = ((std::string)buffer).find("\r\n\r\n") + 4;
    char *t = buffer;
    t = t + bodySize;

    bodySize = body_received - bodySize;

    file.write(t, bodySize);
    memset(buffer, 0, bSize);

    while ((body_received = recv(socketId, buffer, bSize, 0)) > 0) {
        file.write(buffer, body_received);
        memset(buffer, 0, bSize);
    }

    file.close();
}


void HTTPSocket::addHTTPRequest(std::string url, std::string type, std::vector<std::string> params)
{
    addProtocol(url);
    addDomain(url);
    addPath(url);
    addName(url);

    raw_host = gethostbyname( domain.c_str() );
    if (raw_host == NULL)
    {
        std::cout<<"ERROR, no such host";
        exit(0);
    }
    socketId = socket(AF_INET, SOCK_STREAM, 0);

    addr.sin_family = AF_INET;
    addr.sin_port = htons(80);

    bcopy( (char*)raw_host->h_addr, (char*)&addr.sin_addr, raw_host->h_length );

    if( connect( socketId, (struct sockaddr *)&addr, sizeof(addr) ) < 0)
    {
        std::cerr<<"connect error"<<std::endl;
        exit(2);
    }


    std::string request = "";
    request += type + " ";
    request += (std::string)"/" + path + filename;
    request += (std::string)" HTTP/1.0" + "\r\n";
    request += (std::string)"Host: " + domain + "\r\n";
    request += (std::string)"User-Agent: Mozilla/5.0 (Macintosh; Intel Mac OS X x.y; rv:42.0) Gecko/20100101 Firefox/42.0" + "\r\n";
    request += (std::string)"Accept-Language: ru,en-us;q=0.7,en;q=0.3" + "\r\n";
    request += (std::string)"Accept-Charset: windows-1251,utf-8;q=0.7,*;q=0.7" + "\r\n";
    request += "\n";


    const char * message = request.c_str();
    send(socketId, message, request.size(), 0);

    const int chunk_size = 512;
    getFile(chunk_size);
    close(socketId);

}
